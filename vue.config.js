module.exports = {
    // Path en produccion, debe ser la ultima parte del enlace
    publicPath: process.env.NODE_ENV === 'production'
    ? '/~inca-train/'
    // ? '/~inca-train/'
    : '/',
  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: false
    }
  },
  transpileDependencies: [
    'quasar'
  ],
  pwa: {
    name: 'InCA Train'
  }
}
