// Plan
// For creation needs title, nodes, createdBy and Description
{
    id: Number,
    title: String,
    createdBy: String,
    description: String,
    nodes: [ Node ],
    completed: Boolean,
    level: Number,
    lastExec: Date,
    traineeId: Number
}

// Node
// text, editable(clickeable), link(la flechita) y style
// son necesarios por la libreria
// en una base de datos se pueden reemplazar (excepto texto)
// por un solo "estado" que represente a estos 3
// y procesarlos en la app 
{
    id: String,
    text: String,
    next: [ String ],
    redirectTo: String
    editable: Boolean,
    link: String,
    style: String,
}

// Training Session
{
    id: Number,
    blocks: [block]
}

// Block {
    repeat: Number,
    instructions: [ String ],
    linkToVid: ""
}
// User
{
    username: String,
    mail: String,
    trainees: [ Trainee ]
}

// Trainee 
{
    id: Number, 
    name: String,
    species: String,
    gender: String,
    age: Number
}

// Settings
