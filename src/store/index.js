import { createStore } from "vuex"
// TODO: normalize date problems. When you do new Date() it is created in gtm-0 (3 hrs more than Chile)
const styles = {
  completed: "fill:#82E0AA,stroke:#333,stroke-width:2px",
  available: "fill:#F4D03F,stroke:#333,stroke-width:2px",
  unavailable: "fill:#b4b4b4,stroke-width:2px,color:#fff,stroke-dasharray: 5 5"
}

const defaultSettings = {
  repInterval: 1,
  exitPenalty: 1,
  maxPlanLevel: 5,
  spacedRepetitionFunction: { value: 0, label: "incremental" },
  levelAfter: { value: 0, label: "After it's completed" }
}

function incremental (plan, state) {
  return plan.level * state.settings.repInterval
}
function constantValue (plan, state) {
  return state.settings.repInterval
}
// Quizas cambiarlo a una lista de objetos {func: funcion, name: nombre funcion}
const spacedRepetitionFunctions = [
  [incremental, "incremental"],
  [constantValue, "constant value"]
]

// IMPORTANTE
// Actualmente los valores se guardan en localStorage, se configuro
// el state para que revise si existe data y si no crearla, por lo que si
// Se desea actualizar o agregar algun valor extra a lo que ya se tiene,
// es necesario BORRAR el storage existente => localStorage.removeItem('nombreItem')
// Ej: si se desea agregar algo al defaultSettings
export default createStore({
  state: {
    logs: initData("logs"),
    user: {
      username: "InCA User",
      trainees: initData("trainees"),
      mail: "test@mail.com"
    },
    settings: initData("settings"),
    recomendations: [],
    currentDate: new Date(),
    plans: initData("plans"),
    trainingSessions: initData("trainingSessions"),
    lastClickedNode: null
  },
  getters: {
    //   getPlanById
    //   Obtiene un plan y toda su informacion dado el estado y un id
    getPlanById: state => id => {
      return state.plans[state.user.trainees[0].id].find(plan => plan.id === id)
    },
    // getSpacedRepetitionFunctions
    // Retorna la constante spacedRepetitionFunctions con un formato para ser utilizado en las views
    getSpacedRepetitionFunctions: state => {
      return spacedRepetitionFunctions.map((fn, index) => {
        return { value: index, label: fn[1] }
      })
    },
    // calculateNextSession
    // Calcula la fecha de la proxima sesion del plan de entrenamiento, dado el estado y el id del plan
    calculateNextSession: state => planId => {
      const plan = state.plans[state.user.trainees[0].id].find(plan => {
        return plan.id === planId
      })
      const days = spacedRepetitionFunctions[
        state.settings.spacedRepetitionFunction.value
      ][0](plan, state)
      const last = new Date(plan.lastExec).getTime()
      const ret = new Date(last + days * (24 * 60 * 60 * 1000))
      // Necesario para que mantenga el mismo formato que los last exec de los planes
      // Ni idea por que cambia, magias de Javascript
      return JSON.parse(JSON.stringify(ret))
    },
    // getRecomendations
    // Retorna una lista de recomendaciones generadas a partir de los planes que debiesen ser ejecutados en la fecha actual
    getRecomendations: state => {
      try {
        state.recomendations = []
        state.plans[state.user.trainees[0].id].forEach(plan => {
          let x = 0
          if (plan.lastExec !== null) {
            x = Math.floor(
              (state.currentDate.getTime() -
                new Date(plan.lastExec).getTime()) /
                (1000 * 3600 * 24)
            )
          }
          // settings.spacedRepetitionFunction = {value: int, label: nombre de la funcion}
          if (plan.lastExec === null) {
            state.recomendations.push({
              title: plan.title,
              id: plan.id,
              lastExec: plan.lastExec
            })
          } else if (
            spacedRepetitionFunctions[
              state.settings.spacedRepetitionFunction.value
            ][0](plan, state) <= x
          ) {
            state.recomendations.push({
              title: plan.title,
              id: plan.id,
              lastExec: plan.lastExec
            })
          }
        })
      } catch (e) {
        console.log(e)
        state.recomendations = []
      }
      return state.recomendations
    },
    // getSettings
    // Retorna los settings
    getSettings: state => {
      return state.settings
    },
    // getPlans 
    // Retorna los Planes sin nodos (solo informacion) del trainee actual
    getPlans: state => {
      try {
        const r = state.plans[state.user.trainees[0].id].map(plan => {
          return {
            id: plan.id,
            title: plan.title,
            completed: plan.completed,
            level: plan.level,
            lastExec: plan.lastExec,
            visited: plan.visited
          }
        })
        return r
      } catch {
        const r = []
        return r
      }
    },
    // getTrainees
    // retorna los trainees del usuario
    getTrainees: state => {
      return state.user.trainees
    },
    // getTrainingSession
    // retorna la sesion de entrenamiento seleccionada del trainee actual
    getTrainingSession: state => (planId, tsId) => {
      return state.trainingSessions[state.user.trainees[0].id][planId][tsId]
    },
    // deprecado
    getLastClickedNode: state => {
      return state.lastClickedNode
    },
    // getHumanReadableLog
    // Retorna un texto con los logs de un plan en un formato mas sencillo de leer.
    getHumanReadableLog: state => planId => {
      let logElements
      try {
        logElements = state.logs[state.user.trainees[0].id][planId]
      } catch {
        return null
      }
      if (logElements === undefined) {
        return undefined
      }
      let text = ""
      const planTitle = state.plans[state.user.trainees[0].id].find(
        plan => plan.id === planId
      ).title
      // Variable to say: this is the nth session
      let sessionNumber = 1
      logElements.forEach((logElement, index) => {
        const action = logElement.action
        const date = csvDate(logElement.date)
        if (action !== "Exit" && action !== "Start" && action !== "Finished") {
          text += `[${date}] ${action} on ${planTitle} in instruction block ${logElement.block}. Correct: ${logElement.corrects}/${logElement.expected}\n`
        } else if (action === "Exit") {
          text += `[${date}] Premature exit on ${planTitle} in instruction block ${logElement.block}. Reason(s): ${logElement.comments}\n`
        } else if (action === "Start") {
          text += `[${date}] Starting ${planTitle}, Session number: ${sessionNumber} ,session id: ${logElement.session}, current level: ${logElement.level} \n`
          sessionNumber += 1
        } else {
          text += `[${date}] Finished ${planTitle}, session id: ${logElement.session} \n`
        }
      })
      return text
    },
    // downloadLog
    // genera el csv de un log para su descarga. Retorna el archivo
    downloadLog: state => planId => {
      var textFile = null
      const logElements = state.logs[state.user.trainees[0].id][planId]
      if (logElements === undefined) {
        return undefined
      }
      const csvLogs = logElements.map(logElement => {
        return `${csvDate(logElement.date)},${logElement.traineeName},${
          logElement.planId
        },${logElement.level},${logElement.session},${logElement.block},${
          logElement.action
        },${logElement.corrects},${logElement.expected},${
          logElement.comments
        }\n`
      })
      const header =
        "date,traineeName,planId,level,sessionId,block,action,corrects,expected,comments\n"
      const text = [header, ...csvLogs]
      var data = new Blob(text, { type: "text/csv;charset=utf-8;" })

      // If we are replacing a previously generated file we need to
      // manually revoke the object URL to avoid memory leaks.
      if (textFile !== null) {
        window.URL.revokeObjectURL(textFile)
      }

      textFile = window.URL.createObjectURL(data)

      // returns a URL you can use as a href

      return textFile
    },
    // downloadTemplate
    // retorna el texto del template
    downloadTemplate: state => {
      const text = `\\\\ You need to remove the comments before upload 
{"title" : "title",
 "madeBy": "user",
 "description": "desc",
\\\\nodes are what you see in the graph (the squares with arrows)  
"nodes": [
            {
             "id": ,
           \\\\ The text that is shown in the graph 
             "text": "",
             \\\\ A list with the id of the next node(s)
             "next": [],
             \\\\ which training session should the node redirect (id)
             "redirectTo": },
        ],
"trainingSessions": [
            {"id": 0,
            \\\\ list of blocks to follow in the training session, each object has the amount of
            \\\\ times it should be done (repeat) the instructions of the block and
            \\\\ an optional link to a youtube vid, it needs to be a link to embeded video
            \\\\ share button -> embed 
             "blocks": [
                 {"repeat": 0,
                     "instructions": ["do this", "then this"],
                 "linkToVid": ""},
            ]},
         ]
}`
      return text
    },
    // downloadExample
    // Retorna el texto del ejemplo
    downloadExample: state => {
      const text = `{"title" : "Example plan",
"madeBy": "User",
"description": "plan description",
"nodes": [
          {
            "id": 0,
           "text": "Training Session 0",
           "next": [1],
           "redirectTo": 0},
          {"id": 1,
           "text": "Training Session",
           "next": [],
           "redirectTo": 1}
      ],
"trainingSessions": [
          {"id": 0,
           "blocks": [
               {"repeat": 1,
                   "instructions": ["Do this", "Then this"],
               "linkToVid": ""},
              {"repeat": 2,
               "instructions": ["Only this"],
               "linkToVid":""}
          ]},
              {"id": 1,
           "blocks": [
               {"repeat": 1,
                   "instructions": ["Do this", "Then this"],
               "linkToVid": ""},
              {"repeat": 2,
               "instructions": ["Only this"],
               "linkToVid":""}
          ]}
       ]
      }`
      return text
    }
  },
  mutations: {
    //   deprecado
    setLastClickedNode (state, payload) {
      state.lastClickedNode = payload
    },
    // newTrainee: state, payload = {traineeName, age, species, gender}
    // Crea un nuevo trainee para el usuario. Se coloca al principio del listado quedando automaticamente seleccionado
    newTrainee (state, payload) {
      const trainees = state.user.trainees
      let maxId = 0
      if (trainees !== undefined && trainees.length > 0) {
        maxId = Math.max.apply(
          Math,
          trainees.map(function (o) {
            return o.id
          })
        )
      }
      state.user.trainees = [
        {
          id: maxId + 1,
          name: payload.traineeName,
          age: payload.age,
          species: payload.species,
          gender: payload.gender
        },
        ...state.user.trainees
      ]
      localStorage.setItem("trainees", JSON.stringify(state.user.trainees))
    },
    // decreasePlanLevel: state, payload = {planId, decrease}
    // disminuye el nivel de un plan en una cantidad determinada
    decreasePlanLevel (state, payload) {
      const plan = state.plans[state.user.trainees[0].id].find(
        plan => plan.id === payload.planId
      )
      plan.level =
        plan.level - payload.decrease > 0 ? plan.level - payload.decrease : 1
      localStorage.setItem("plans", JSON.stringify(state.plans))
    },
    // deletePlan: state, planId
    // Elimina un plan y toda su informacion (sesiones y logs)
    deletePlan (state, planId) {
      const index = state.plans[state.user.trainees[0].id].findIndex(plan => {
        return plan.id === planId
      })
      state.plans[state.user.trainees[0].id].splice(index, 1)
      localStorage.setItem("plans", JSON.stringify(state.plans))
      delete state.trainingSessions[state.user.trainees[0].id][planId]
      localStorage.setItem(
        "trainingSessions",
        JSON.stringify(state.trainingSessions)
      )
      if (state.logs[state.user.trainees[0].id][planId] !== undefined) {
        delete state.logs[state.user.trainees[0].id][planId]
        localStorage.setItem("logs", JSON.stringify(state.logs))
      }
    },
    // completedSession: state, payload = {planId, nodeId}
    // Procesa una sesion completada, dando acceso a proximos nodos en caso de que existan o dejando el plan como completado
    completedSession (state, payload) {
      const planId = payload.planId
      const nodeId = payload.nodeId
      const p = state.plans[state.user.trainees[0].id].find(
        plan => plan.id === planId
      )
      p.lastExec = new Date()
      const n1 = p.nodes.find(node => node.id === nodeId)
      n1.style = styles.completed
      n1.link = "-->"
      // levelAfter
      if (
        (state.settings.levelAfter.value === 0 &&
          p.completed &&
          p.level < state.settings.maxPlanLevel) ||
        (state.settings.levelAfter.value === 1 &&
          p.level < state.settings.maxPlanLevel)
      ) {
        p.level += 1
      }
      if (n1.next.length > 0) {
        n1.next.forEach(nodeId => {
          const t = p.nodes.find(node => node.id === nodeId)
          const prevNodes = p.nodes.filter(
            node => node.next.indexOf(nodeId) > -1
          )
          if (
            prevNodes.filter(node => node.style !== styles.completed).length ===
            0
          ) {
            t.style = styles.available
            t.editable = true
          }
        })
      } else {
        p.completed = true
      }
      state.recomendations = []
      localStorage.setItem("plans", JSON.stringify(state.plans))
    },
    // saveSettings state, payload = settings
    // guarda los cambios a los settings
    // Actualmente se guarda con un click, pero esto no es necesario (se puede hacer automaticamente).
    // Se dejo de esta manera solo por claridad y testeo en el prototipo
    saveSettings (state, payload) {
      state.settings = payload
      localStorage.setItem("settings", JSON.stringify(payload))
    },
    // deleteAllData
    // Borra toda la informacion de la aplicacion, dejandola en el mismo estado de cuando se instalo (reset)
    deleteAllData (state) {
      localStorage.removeItem("trainees")
      localStorage.removeItem("plans")
      localStorage.removeItem("logs")
      localStorage.removeItem("trainingSessions")
      localStorage.removeItem("settings")
      state.user.trainees = initData("trainees")
      state.plans = initData("plans")
      state.logs = initData("logs")
      state.trainingSessions = initData("trainingSessions")
      state.settings = initData("settings")
    },
    // deprecada
    updateExitPenalty (state, payload) {
      state.settings.exitPenalty = payload
      localStorage.setItem("settings", JSON.stringify(state.settings))
    },
    // selectTrainee
    // Cambia la posicion del trainee para que el seleccionado pase a ser el primero de la lista
    selectTrainee (state, index) {
      [state.user.trainees[0], state.user.trainees[index]] = [
        state.user.trainees[index],
        state.user.trainees[0]
      ]
      localStorage.setItem("trainees", JSON.stringify(state.user.trainees))
    },
    // logAction: state, payload {planId, tsId, action, text (for exit), block, correct, expected}
    // Guarda una accion ejecutada. Es un poco ineficiente (se llama cada vez que algo ocurre)
    // Quizas cambiarlo a que procese un listado, pero en ese caso hay que cambiar el "undo" en la sesion de entrenamiento
    logAction (state, payload) {
      const date = new Date().toISOString()
      const plan = state.plans[state.user.trainees[0].id].find(
        plan => plan.id === payload.planId
      )
      if (state.logs[state.user.trainees[0].id] === undefined) {
        state.logs[state.user.trainees[0].id] = {}
      }
      if (state.logs[state.user.trainees[0].id][plan.id] === undefined) {
        state.logs[state.user.trainees[0].id][plan.id] = []
      }
      const action = payload.action
      const logElement = {
        date: date,
        traineeName: state.user.trainees[0].name,
        planId: payload.planId,
        level: plan.level,
        session: payload.tsId,
        action: action,
        corrects: payload.correct,
        expected: payload.expected,
        comments: payload.text,
        block: payload.blockNumber
      }
      state.logs[state.user.trainees[0].id][plan.id].push(logElement)
      localStorage.setItem("logs", JSON.stringify(state.logs))
    },
    // addPlan
    // Crea un plan para el trainee actual
    addPlan (state, plan) {
      const plans = state.plans[state.user.trainees[0].id]
      let maxId = 0
      if (plans !== undefined && plans.length > 0) {
        maxId = Math.max.apply(
          Math,
          state.plans[state.user.trainees[0].id].map(function (o) {
            return o.id
          })
        )
      }
      plan.id = (maxId + 1).toString()
      plan.completed = false
      plan.level = 1
      plan.lastExec = null
      // plan.traineeId = state.user.trainees[0].id
      plan.nodes.forEach((node, index) => {
        node.id = node.id.toString()
        node.next = node.next.map(id => {
          return id.toString()
        })
        node.redirectTo = "ts:" + node.redirectTo.toString()
        node.editable = index === 0
        node.link = "-.->"
        node.style = index === 0 ? styles.available : styles.unavailable
      })
      const trainingSessions = plan.trainingSessions
      delete plan.trainingSessions
      if (state.trainingSessions[state.user.trainees[0].id] === undefined) {
        state.trainingSessions[state.user.trainees[0].id] = {}
      }
      state.trainingSessions[state.user.trainees[0].id][plan.id] = {}
      trainingSessions.forEach(ts => {
        ts.blocks.forEach((t, index) => {
          t.id = index
        })
        state.trainingSessions[state.user.trainees[0].id][plan.id][ts.id] =
          ts.blocks
      })
      if (state.plans[state.user.trainees[0].id] === undefined) {
        state.plans[state.user.trainees[0].id] = [plan]
      } else {
        state.plans[state.user.trainees[0].id].push(plan)
      }
      localStorage.setItem("plans", JSON.stringify(state.plans))
      localStorage.setItem(
        "trainingSessions",
        JSON.stringify(state.trainingSessions)
      )
    },
    // deleteTrainee
    // Borra al trainee y toda su informacion (planes, sesiones y logs)
    deleteTrainee (state, selected) {
      const traineeId = state.user.trainees[selected.value].id
      state.user.trainees.splice(selected.value, 1)
      localStorage.setItem("trainees", JSON.stringify(state.user.trainees))
      delete state.plans[traineeId]
      delete state.trainingSessions[traineeId]
      delete state.logs[traineeId]
      localStorage.setItem("plans", JSON.stringify(state.user.trainees))
      localStorage.setItem(
        "trainingSessions",
        JSON.stringify(state.user.trainees)
      )
      localStorage.setItem("logs", JSON.stringify(state.user.trainees))
    },
    // saveTraineeEdit
    // guarda los cambios de un trainee
    // Estos cambios son automaticos (como se menciono en la parte de settings), por lo que esta funcion 
    // "graba" en el sitema el cambio realizado
    saveTraineeEdit (state) {
      localStorage.setItem("trainees", JSON.stringify(state.user.trainees))
    },
    // cancelTraineeEdit
    // Cancela la edicion
    cancelTraineeEdit (state) {
      state.user.trainees = initData("trainees")
    }
  },
  actions: {
    uploadPlan ({ commit, state }, file) {
      try {
        const parsed = JSON.parse(file)
        if (parsed.title === undefined || typeof parsed.title !== "string") {
          console.log("no title o no string")
          return false
        }
        if (parsed.madeBy === undefined || typeof parsed.madeBy !== "string") {
          console.log("no made by o no string")
          return false
        }
        if (
          parsed.description === undefined ||
          typeof parsed.description !== "string"
        ) {
          console.log("no description o no string")
          return false
        }
        if (parsed.nodes === undefined || !validateNodes(parsed.nodes)) {
          console.log("no nodes o no valido")
          return false
        }
        if (
          parsed.trainingSessions === undefined ||
          parsed.trainingSessions.filter(ts => {
            if (ts.id === undefined || typeof ts.id !== "number") {
              console.log("error ts id")
              return false
            }
            if (
              ts.blocks === undefined ||
              !validateTrainingSessions(ts.blocks)
            ) {
              return false
            }
            return true
          }).length !== parsed.trainingSessions.length
        ) {
          return false
        }
        commit("addPlan", parsed)
        return true
      } catch (e) {
        console.log(e)
        return false
      }
    }
  },
  modules: {}
})

// validateNodes: nodes
// Valida que el formato de los nodos este correcto en el json
function validateNodes (nodes) {
  try {
    const r = nodes.filter(node => {
      if (node.id === undefined || typeof node.id !== "number") {
        console.log("no node id o no number")
        return false
      }
      if (node.text === undefined || typeof node.text !== "string") {
        console.log("no node text o no string")
        return false
      }
      if (
        node.next === undefined ||
        node.next.filter(n => typeof n !== "number").length > 0
      ) {
        console.log("no node next o tipos raros")
        return false
      }
      if (
        node.redirectTo === undefined ||
        typeof node.redirectTo !== "number"
      ) {
        console.log("no node redirectTo , no number")
        return false
      }
      return true
    })
    return r.length > 0
  } catch {
    return false
  }
}

// validateTrainingSessions
// valida que el formato de las sesiones de entrenamiento esten correctas en el json
function validateTrainingSessions (sessions) {
  try {
    const r = sessions.filter(session => {
      if (session.repeat === undefined || typeof session.repeat !== "number") {
        console.log("no session repeat o no string")
        return false
      }
      if (
        session.instructions === undefined ||
        session.instructions.length === 0 ||
        session.instructions.filter(s => typeof s !== "string").length > 0
      ) {
        console.log("no session text o no string")
        return false
      }
      if (
        session.linkToVid === undefined ||
        typeof session.linkToVid !== "string"
      ) {
        console.log("no session text o no string")
        return false
      }
      return true
    })
    return r.length > 0
  } catch (e) {
    console.log(e)
    return false
  }
}

// initData
// Carga la informacion a la aplicacion desde el localStorage. En caso de no haber, crea las estructuras vacias.
function initData (data) {
  let d = localStorage.getItem(data)
  if (d === null) {
    if (data === "settings") {
      d = defaultSettings
    } else if (
      data === "plans" ||
      data === "trainingSessions" ||
      data === "logs"
    ) {
      d = {}
    } else {
      d = []
    }
    localStorage.setItem(data, JSON.stringify(d))
    return d
  } else {
    return JSON.parse(d)
  }
}

// csvDate
// Formatea las fechas desde javascript a un formato amigable con el formato csv y las hojas de calculo (excel)
function csvDate (date) {
  const d = new Date(date)
  const pad = n => n.toString().padStart(2, "0")
  return `${d.getFullYear()}-${pad(d.getMonth() + 1)}-${pad(d.getDate())} ${pad(
    d.getHours()
  )}:${pad(d.getMinutes())}:${pad(d.getSeconds())}`
}
