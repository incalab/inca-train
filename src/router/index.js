import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
// import store from '../store'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/plan/:id',
    name: 'Plan',
    component: () => import('../views/PlanView.vue')
  },
  {
    path: '/train/:planId/:tsId',
    name: 'Session',
    component: () => import('../views/Training.vue')
  },
  {
    path: '/planlist',
    name: 'Plan List',
    component: () => import('../views/PlanList.vue')
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import('../views/Settings.vue')
  },
  {
    path: '/planDetails/:planId',
    name: 'Plan Details',
    component: () => import('../views/PlanDetails.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/log',
    name: 'Logs',
    component: () => import('../views/Log.vue')
  },
  {
    path: '/trainees',
    name: 'Trainees',
    component: () => import('../views/Trainees.vue')
  },
  {
    path: '/newTrainee',
    name: 'New Trainee',
    component: () => import('../views/NewTrainee.vue')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

// Obliga a revisar login antes de entrar a cada view
// router.beforeEach((to, from, next) => {
//   if (to.name !== 'Login' && !store.state.loged) {
//     next({ name: 'Login' })
//   } else next()
// })
export default router
