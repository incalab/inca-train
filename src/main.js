import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import Vue3Mermaid from 'vue3-mermaid'
import { Quasar } from 'quasar'
import quasarUserOptions from './quasar-user-options'
import title from './mixins/title'

createApp(App).use(Quasar, quasarUserOptions).use(store).use(router).use(Vue3Mermaid).mixin(title).mount('#app')
